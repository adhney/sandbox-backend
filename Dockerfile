# Use the official Golang image as the base image
FROM golang:latest

# Set the working directory inside the container
WORKDIR /app

# Install Air for hot reloading
RUN go install github.com/cosmtrek/air@latest

# Copy the entire backend directory into the container
COPY . .

# Expose the port that your Gin application is running on
EXPOSE 8080

# Run the application with Air for hot reloading
CMD ["air"]
  